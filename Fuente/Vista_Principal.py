from tkinter import ttk
from tkinter import Menu
from tkinter import Text
from tkinter import Scrollbar
from tkinter import Label
from tkinter import Entry
from tkinter import Button
from tkinter import filedialog
import codecs
import tkinter as tk
import os
import string
from Botonera import Paleta
import xml.etree.cElementTree as ET

from ManejaArchivos import ManejaArchivos
from Gramatica import Gramatica
from ManejaGramatica import ManejaGramatica
from Algoritmos import Algoritmos


# from Modelo.ManejaArchivos import ManejaArchivos
# sys.path.append(os.path.abspath("/Fuente/Modelo"))
# import ManejaArchivos


class VentanaPrincipal(ttk.Frame):
    def saveFile(self):
        manejaArchivos = ManejaArchivos()
        texto = str(self.entrada_hilera.get(1.0, tk.END))
        ubicacion = filedialog.asksaveasfile(mode='w', title="Save file", defaultextension=".xml", filetypes=(("Extensible Markup Language", "*.xml"), ("Plain text", "*.txt")))
        name, ext = os.path.splitext(ubicacion.name)
        #print(ext)
        if ext == ".xml":
            manejaArchivos.escribeXML(texto, ubicacion.name)
        if ext == ".txt":
            manejaArchivos.escribeTXT(texto, ubicacion.name)

    def openArchive(self):
        pathGrammar = filedialog.askopenfilename(initialdir="/", title="Open file", filetypes=(("xml", "*.xml"), ("txt", "*.txt")))
        if pathGrammar is None and len(pathGrammar) >= 4:
            return
        # try:
        ext = pathGrammar[-4:]
        if ext == ".txt":
            grammar = open(pathGrammar, encoding='UTF-8').read().strip()
            self.entrada_hilera.insert("end", grammar)
        if ext == ".xml":
            # except ValueError:
            self.entrada_hilera.insert("end", self.manejaArchivo.abreXML(pathGrammar))

    def openInputFile(self):
        pathGrammar = filedialog.askopenfilename(initialdir="/", title="Select file", filetypes=(("txt", "*.txt"), ("Others", "*.others")))
        grammar = codecs.open(pathGrammar, encoding='utf-8')
        self.entradaMultiple.insert("end", grammar.read())

    def cerrarPrograma(self):
        main_window.destroy()

    def correrHilera(self):      
        algoritmo = self.entrada_hilera.get(1.0, tk.END)
        hilera = self.entrada.get()
        if hilera:
            resultado = self.manejaGramatica.ejecutarHilera(algoritmo, hilera)
            self.salida_hilera.insert("end", '>>> ' + resultado + '\n')
                
    def correrHileraDebug(self):        
        algoritmo = self.entrada_hilera.get(1.0, tk.END)
        hilera = self.entrada.get()   
        if hilera:
            resultado = self.manejaGramatica.ejecutarHileraDebug(algoritmo, hilera)
            self.salida_hilera.insert("end", '>>> ' + resultado + '\n')
            
    def correrMultiplesHileras(self):
        algoritmo = self.entrada_hilera.get(1.0, tk.END)
        hileras = self.entradaMultiple.get(1.0, tk.END)
        if hileras:
            hileras = hileras.split()
            for hilera in hileras:
                resultado = self.manejaGramatica.ejecutarHilera(algoritmo, str(hilera.strip()))
                self.salida_hilera.insert("end", '>>> ' + resultado + '\n')

    def asignaAlgoritmo(self, value):
        if not self.combo.get() == 'None':
            self.entrada_hilera.insert("end", self.algoritmos.returnaAlgoritmo(self.ComboValues.index(self.combo.get())))
        

    def __init__(self, main_window):
        super().__init__(main_window)
        main_window.title("MarkPar")
        self.ComboValues = ['None', 'Invertir una hilera', 'Duplicar una hilera',
                       'Es palíndromo',
                       'Convertir un número binario en su equivalente unario',
                       'suma de números binarios', 'Triplicar números binarios',
                       'Multiplicación de números binarios arbitrarios', 'Suma de números decimales',
                       'Eliminar los caracteres repetidos',
                       'Verificar la estructura de una hilera que contenga únicamente paréntesis']

        # self.gramatica = Gramatica()
        self.manejaArchivo = ManejaArchivos()
        self.manejaGramatica = ManejaGramatica()
        self.algoritmos = Algoritmos()

        self.menubar = Menu(main_window)

        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu1 = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Open", command=self.openArchive)
        self.filemenu.add_command(label="Save", command=self.saveFile)
        self.filemenu.add_command(label="Exit", command=self.cerrarPrograma)
        self.filemenu1.add_command(label="Greek alphabet", command=self.abrebotonera)

        self.scroll = Scrollbar(self, orient="vertical")
        self.scroll2 = Scrollbar(self, orient="vertical")

        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.menubar.add_cascade(label="Special characters", menu=self.filemenu1)

        self.labelInput = Label(self, text="Input", height=1, width=5, font="Helvetica 12 bold")
        self.labelInput.place(x=5, y=1)

        self.entrada_hilera = Text(self)
        self.entrada_hilera.place(x=10, y=25, height=400, width=800)
        self.entrada_hilera.configure(yscrollcommand=self.scroll.set)

        self.scroll.configure(command=self.entrada_hilera.yview)

        self.labelOutput = Label(self, text="Output", height=1, width=5, font="Helvetica 12 bold")
        self.labelOutput.place(x=5, y=425)

        self.salida_hilera = Text(self, bg="#000000", fg="#FFFFFF")  # ,state='disabled'
        self.salida_hilera.place(x=10, y=450, height=200, width=800)
        self.salida_hilera.configure(yscrollcommand=self.scroll2.set)
        self.salida_hilera.tag_config('console', background="black", foreground="white")

        self.scroll2.configure(command=self.salida_hilera.yview)

        self.labelFondoHilera = Label(self, text="", bg="#F5F5F5", height=10, width=55)
        self.labelFondoHilera.place(x=820, y=10)

        self.labelRun = Label(self, text="Run", bg="#F5F5F5", font="Helvetica 12 bold")
        self.labelRun.place(x=822, y=22)

        self.entrada = Entry(self, width=55, bd=2)
        self.entrada.place(x=830, y=52)

        self.runButton = Button(self, text="Run", height=2, width=8, command=self.correrHilera)
        self.runButton.place(x=830, y=82)

        self.debugButton = Button(self, text="Debug", height=2, width=8, command=self.correrHileraDebug)
        self.debugButton.place(x=1090, y=82)

        self.labelFondoMultiple = Label(self, text="", bg="#F5F5F5", height=16, width=55)
        self.labelFondoMultiple.place(x=820, y=190)

        self.labelMultipleRun = Label(self, text="Multiple Run", bg="#F5F5F5", font="Helvetica 12 bold")
        self.labelMultipleRun.place(x=820, y=205)

        self.loadInputButton = Button(self, text="Load Inputs", height=1, width=8, command=self.openInputFile)
        self.loadInputButton.place(x=940, y=200)

        self.entradaMultiple = Text(self)
        self.entradaMultiple.place(x=830, y=240, height=125, width=350)
        self.entradaMultiple.configure(yscrollcommand=self.scroll.set)

        self.runMultiButton = Button(self, text="Run", height=2, width=8, command=self.correrMultiplesHileras)
        self.runMultiButton.place(x=980, y=380)

        self.labelFondoAlgoritmo = Label(self, text="", bg="#F5F5F5", height=10, width=55)
        self.labelFondoAlgoritmo.place(x=820, y=460)

        self.labelCargarAlgoritmo = Label(self, text="Premade algorithms", bg="#F5F5F5", font="Helvetica 12 bold")
        self.labelCargarAlgoritmo.place(x=820, y=465)

        self.combo = ttk.Combobox(self, values=self.ComboValues, width=60)
        self.combo.place(x=820, y=510)
        self.combo.current(0)
        self.combo.bind("<<ComboboxSelected>>", self.asignaAlgoritmo)

        """self.prueba_label=tk.Label(self,text="Ejecución de un algoritmo", font='Helvetica 18 bold')
        self.prueba_label.grid(row=0,column=0)
        self.label_hilera=tk.Label(self,text="Hilera:")
        self.label_hilera.grid(row=1,column=0)
        self.label_algoritmo=tk.Label(self,text="Algoritmo:")
        self.label_algoritmo.grid(row=2,column=0)
        self.combo.grid(row=2,column=1)
        self.combo["values"] = ["Invertir", "Duplicar", "Es Palíndromo", "Convertir a binario"]
        self.label_salida=tk.Label(self,text="Salida:")
        self.label_salida.grid(row=3,column=0)
        self.entrada_salida=tk.Entry(self)
        self.entrada_salida.grid(row=3,column=1)
        self.button = tk.Button(self,text="Run")
        self.button.grid(row=4,column=0)"""
        main_window.config(menu=self.menubar)
        main_window.configure(width=1250, height=720)
        self.place(width=1250, height=720)

    def abrebotonera(self):
        paleta = Paleta(tk.Toplevel(self), self)


if __name__ == "__main__":
    main_window = tk.Tk()
    app = VentanaPrincipal(main_window)
    app.mainloop()
