MarkPar  
Markov Algorithm tool  
Create own grammars using markers, symbols and variables.  
Run grammars on normal and debug mode.  

General syntax:  
#markers : Accepts etters from the greek alphabet, in order to insert them, open the extra button window and click on the desired character.  

#symbols : Accepts letters and numbers from A-Z, a-z, 0-9.  

#vars : Accepts letters xywz.  

Rule syntax:  
(_) : If there is a string inside parenthesis at the beginning of the rule, it will be taken as the rule ID.  
String before '->' : the pattern to look for.  
-> : Separates the pattern from the replacement.  
String after '->' : the replacement for the desired pattern.  
'.' after '->' : Indicates that this will be the last rule executed.  
'Λ' after '->' : Indicates that the replacement will be an empty string.  
$(_) : If there is a string inside a pattern at the end of the rule, it will be taken as an instruccion indicating which rule to execute next.  

Any line starting with % will be considered a comment.  
If the symbols are not defined, the default (A-Za-z0-9) will be loaded.  
If the markers are not defined, the default (lowercase alpha to gamma) will be loaded.  
If the variables are not defined, the default (xywz) will be loaded.  

Usage example:  
%Duplicate string  
%Note that there are no symbols, therefore any character from A-Za-z0-9 can be used.  
#markers β  
#vars x  
(P1) βx -> xxβ $(P1)  
(P2) β -> .Λ  
(P3) x -> βx $(P1)  
