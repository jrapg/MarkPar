class Gramatica:
    def __init__(self):
        self.__simbolos = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ0123456789()'
        self.__marcadores = 'αβγδ'
        self.__variables = 'wxyz'
        self.__comentarios = ''
        self.__reglas = []

    def setSimbolos(self, nuevosSimbolos):
        if nuevosSimbolos:
            self.__simbolos = nuevosSimbolos


    def setMarcadores(self, nuevosMarcadores):
        if nuevosMarcadores:
          self.__marcadores = nuevosMarcadores


    def setVariables(self, nuevasVariables):
        if nuevasVariables:
            self.__variables = nuevasVariables


    def setComentarios(self, nuevosComentarios):
        if nuevosComentarios:
            self.__comentarios = nuevosComentarios

    
    def setReglas(self, nuevaListaReglas):
        if nuevaListaReglas:
            self.__reglas = nuevaListaReglas


    def getSimbolos(self):
        return self.__simbolos


    def getMarcadores(self):
        return self.__marcadores


    def getVariables(self):
        return self.__variables


    def getComentarios(self):
        return self.__comentarios


    def getReglas(self):
        return self.__reglas


    def demeReglaConEtiqueta(self, etiqueta):
        etiqueta = etiqueta.replace('$', '')    
        for regla in self.__reglas:            
            if regla.getEtiqueta() == etiqueta:
                return regla
        return 0