class Algoritmos:
    def __init__(self):
        self.invierteHilera = ''

        self.duplicaHilera = '''%COMENTARIO 1
#symbols dsao
#markers β
#vars x
(P1) βx -> xxβ $(P1)
(P2) β -> .Λ
(P3) x -> βx $(P1)'''

        self.palindromo = ''

        self.binarioUnitario =  '''#symbols 1d0 
(p1) "1" -> 0d $(p1)
(p2) "d0" -> 0dd $(p2)
(p3) "0" ->      $(p3)'''

        self.sumaBinarios = '''#symbols 10sr
#markers βρτπ
#vars x
(P1) x -> βx $(P2)
(P2) βx -> xβ $(P2)
(P3) ρx -> xρ $(P3)
(P4) ρβ -> βρ $(P9)
(P5) τx -> xτ $(P5)
(P6) τβ -> βτ $(P9)
(P7) "0s" -> sρ $(P3)
(P8) "1s" -> sτ $(P5)
(P9) "0"β -> βρ $(P11)
(P10) "1"β -> βτ $(P11)
(P11) ρρπ -> 1 $(P7) 
(P12) ττπ -> π1 $(P7) 
(P13) τρπ -> π0 $(P7)
(P14) ρτπ -> π0 $(P7)
(P15) ρρ -> 0 $(P7)
(P16) ττ -> π0 $(P7)
(P17) τρ -> 1 $(P7)
(P18) ρτ -> 1 $(P7) 
(P19) π -> 1 $(P19)
(P20) β -> .r'''

        self.triplicaBinario = '''#symbols 10
#markers β
#vars x
(P1) "11" -> .1001
(P2) "10" -> 110 $(P3)
(P3) "01" -> .11'''

        self.multiplicaBinario = ''

        self.sumaDecimales = '''#symbols 1s
#markets β
#vars x
(P1) "s" -> .'''
        
        self.eliminaRepetido = ''

        self.parentesisCorrectos = ''

        self.switcher = {
            1: self.getInvierteHilera,
            2: self.getDuplicaHilera,
            3: self.getPalindromo,
            4: self.getBinarioUnitario,
            5: self.getSumaBinarios,
            6: self.getTriplicaBinario,
            7: self.getMultiplicaBinario,
            8: self.getSumaDecimales,
            9: self.getEliminaRepetido,
            10: self.getParentesisCorrectos
        }      
    
    def getInvierteHilera(self):
        return self.invierteHilera

    def getDuplicaHilera(self):
        return self.duplicaHilera
    
    def getPalindromo(self):
        return self.palindromo
    
    def getBinarioUnitario(self):
        return self.binarioUnitario
    
    def getSumaBinarios(self):
        return self.sumaBinarios
    
    def getTriplicaBinario(self):
        return self.triplicaBinario

    def getMultiplicaBinario(self):
        return self.multiplicaBinario
    
    def getSumaDecimales(self):
        return self.sumaDecimales

    def getEliminaRepetido(self):
        return self.eliminaRepetido
    
    def getParentesisCorrectos(self):
        return self.parentesisCorrectos

    def returnaAlgoritmo(self, pos):
        return self.switcher.get(pos)()

        