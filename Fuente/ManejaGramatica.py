import re
import Regla
import Gramatica

class ManejaGramatica:
    #Las 'r' indican que la hilera se manejará literalmente como está, si tiene
    #algún caracter especial, no se aplicará (\n no significaría nueva línea)
    def __init__(self):
        self.__simbolos = r'(?mx)(?P<simbolos> (?<=\#symbols)\s+([a-zA-Z0-9_\(\)])+)'
        self.__marcadores = r'(?mx)(?P<marcadores>(?<=\#markers)(\s)+[αβγδεζηθικλμνξοπρστυφχψω]+)'
        self.__variables = r'(?mx)(?P<variables>(?<=\#vars)(\s)+[a-z]+)'
        self.__comentarios = r'(?mx)(?P<comentario>(?<=\%)(.)*)'
        self.__reglas = None


    #Retornan las variables iniciadas en la creacion de la clase
    def getRgxSimbolos(self):
        return self.__simbolos


    def getRgxMarcadores(self):
        return self.__marcadores


    def getRgxVariables(self):
        return self.__variables


    def getRgxComentarios(self):
        return self.__comentarios


    def getRgxReglas(self):
        return self.__reglas


    def creaRgxReglas(self, gramatica):
        self.__reglas = r'''(?mx)
           ^(?P<reglas>
           (?P<etiqueta>(\(([a-zA-Z0-9_])+\)\s)*)
           (?P<patron>(([Λ])?|((\"([''' + gramatica.getSimbolos() + r'\ ])+\")*)*|(([''' + gramatica.getVariables() + r'])*)*|((['+ gramatica.getMarcadores() + r'''])*)*)+)\s+->\s+(?P<terminal> (\.))?(?P<reemplazo>(([Λ])?|(([''' + gramatica.getVariables() + r'\ ])*)*|(([''' + gramatica.getSimbolos() + r'\ ])*)*|(([' +gramatica.getMarcadores() + r'''\ ])*)*)+)(?P<instruccion>(\s+[$]\(([a-zA-Z0-9_]+)\))*))$'''


    #Estos metodos obtienen todas las instancias u ocurrencias de las reglas indicadas
    def obtienecomentarios(self, gramatica):
        return [(matchObj.group('comentario'))
            for matchObj in re.finditer(self.__comentarios, gramatica)
            if matchObj.group('comentario')]


    def obtienesimbolos(self, gramatica):
        return [matchObj.group('simbolos')
            for matchObj in re.finditer(self.__simbolos, gramatica)
            if matchObj.group('simbolos')]


    def obtienemarcadores(self, gramatica):
        return [matchObj.group('marcadores')
            for matchObj in re.finditer(self.__marcadores, gramatica)
            if matchObj.group('marcadores')]


    def obtienevariables(self, gramatica):
        return [matchObj.group('variables')
            for matchObj in re.finditer(self.__variables, gramatica)
            if matchObj.group('variables')]


    def obtienereglas(self, gramatica):
        return [matchObj.group('reglas')
            for matchObj in re.finditer(self.__reglas, gramatica)
            if matchObj.group('reglas')]


    def reemplaza(self, hilera, gramatica):
        contador = 0
        tamano = len(gramatica.getReglas()) 
        while True:           
            if contador >= tamano:
                break
            regla = gramatica.getReglas()[contador]
            if regla.getPatron() == 'Λ':
                reemplazo = regla.getReemplazo()
                hilera = reemplazo + hilera
                contador += 1
            else:
                encuentra = re.search(regla.getPatron(), hilera)             
                if encuentra:
                    terminal = regla.getTerminal()
                    char = encuentra.group(0)                    
                    reemplazo = self.limpioReemplazo(regla.getReemplazo(), char, gramatica.getVariables(),gramatica.getMarcadores())                  
                    if reemplazo == 'Λ':
                        hilera = re.sub(regla.getPatron(), '', hilera, 1)
                    else:
                        hilera = re.sub(regla.getPatron(), reemplazo, hilera, 1)                
                    if terminal:
                        break
                    if regla.getInstruccion():                              
                        contador = gramatica.getReglas().index(gramatica.demeReglaConEtiqueta(regla.getInstruccion()))
                    else:
                        if contador < tamano:
                            contador += 1
                else:
                    if contador < tamano:
                        contador += 1
                    else:
                        contador = 0          
        return hilera

    def remplazaDebug(self, hilera, gramatica, reglas):
        contador = 0
        tamano = len(gramatica.getReglas())
        resultado = ''
        contadorTotal = 1
        while True:    
            if contador >= tamano:
                break                  
            regla = gramatica.getReglas()[contador]
            if regla.getPatron() == 'Λ':
                resultado = resultado + 'Step #' + str(contadorTotal) + '"\n' + 'Rule: ' + str(reglas[contador][1])
                if reglas[contador][2]:
                    resultado = resultado + ' -> ' + str(reglas[contador][2]) + '\n'
                else:
                    resultado = resultado + ' -> ' + str(reglas[contador][4]) + '\n'
                resultado = resultado + 'Before: ' + hilera + '\n'
                reemplazo = regla.getReemplazo()
                hilera = reemplazo + hilera
                resultado = resultado + 'After: ' + hilera + '\n' + '\n'
                contador += 1
            else:
                encuentra = re.search(regla.getPatron(), hilera)
                if encuentra:
                    resultado = resultado + 'Step #' + str(contadorTotal) + '"\n' + 'Rule: ' + str(reglas[contador][1])
                    if reglas[contador][2]:
                        resultado = resultado + ' -> ' + str(reglas[contador][2]) + '\n'
                    else:
                        resultado = resultado + ' -> ' + str(reglas[contador][4]) + '\n'
                    resultado = resultado + 'Before: ' + hilera + '\n'
                    terminal = regla.getTerminal()
                    char = encuentra.group(0)
                    reemplazo = self.limpioReemplazo(regla.getReemplazo(), char, gramatica.getVariables(),gramatica.getMarcadores())
                    if reemplazo == 'Λ':
                        hilera = re.sub(regla.getPatron(), '', hilera, 1)
                    else:
                        hilera = re.sub(regla.getPatron(), reemplazo, hilera, 1)
                    resultado = resultado + 'After: ' + hilera + '\n' + '\n'
                    if terminal:
                        break
                    if regla.getInstruccion():
                        contador = gramatica.getReglas().index(gramatica.demeReglaConEtiqueta(regla.getInstruccion()))
                    else:
                        if contador < tamano:
                            contador += 1
                    contadorTotal += 1
                else:
                    if contador < tamano:
                        contador += 1
                    else:
                        contador = 0
        return resultado

    #Estos compilan las ocurrencias indicadas y las une en una sola
    def compilacomentarios(self, gramatica):
        commentlist = self.obtienecomentarios(gramatica)
        if not commentlist:
            return ""
        result = ''.join(commentlist)
        return result


    def compilasimbolos(self, gramatica):
        symbollist = self.obtienesimbolos(gramatica)
        if not symbollist:
            return ""
        result = ''.join(symbollist)
        return ''.join(set(result.replace(" ", "")))


    def compilamarcadores(self, gramatica):
        markerlist = self.obtienemarcadores(gramatica)
        if not markerlist:
            return ""
        result = ''.join(markerlist)
        return ''.join(set(result.replace(" ", "")))


    def compilavariables(self, gramatica):
        variablelist = self.obtienevariables(gramatica)
        if not variablelist:
            return ""
        result = ''.join(variablelist)
        return ''.join(set(result.replace(" ", "")))



    def compilaReglas(self, gramatica):
            return [(matchobj.group('etiqueta'), matchobj.group('patron'), matchobj.group('reemplazo'), matchobj.group('instruccion'), matchobj.group('terminal'))
                    for matchobj in re.finditer(self.__reglas, gramatica)
                    if matchobj.group('reglas')]

    def creaListaDeElementosRegla(self, gramaticaTexto, variables, simbolos):
        listaDeReglas = self.compilaReglas(gramaticaTexto)
        listaResultado = []
        for elemento in listaDeReglas:
            regla = Regla.Regla(elemento[0],
                                self.acomodoPatron(elemento[1],
                                                   variables,
                                                   simbolos),
                                elemento[2],
                                elemento[3],
                                elemento[4])
            listaResultado.append(regla)
        return listaResultado

    def creaListaDeElementosReglaParaGuardar(self, gramaticaTexto):
        listaDeReglas = self.compilaReglas(gramaticaTexto)
        listaResultado = []
        for elemento in listaDeReglas:
            regla = Regla.Regla(elemento[0],
                                elemento[1],
                                elemento[2],
                                elemento[3],
                                elemento[4])
            listaResultado.append(regla)
        return listaResultado

    #Convierte el patron de las reglas en reglas de expresiones regulares (para reemplazar variables por simbolos, etc)
    def acomodoPatron(self, patron, variable, simbolos):
        if patron != 'Λ':
            nPatron = patron.replace('(', '\(')
            nPatron = nPatron.replace(')', '\)')
            for var in variable:
                pos = nPatron.find(var)
                if pos >= 0:
                    nPatron = nPatron.replace(var, '([' + simbolos + '])')
                    break
            nPatron = nPatron.replace("\"", '')
            return r'('+nPatron+'){1}'
        else:
            return patron

    def limpioReemplazo(self, reemplazo, caracter, variables, marcadores):
        resultado = reemplazo
        for letraGriega in marcadores:
            if letraGriega in caracter:
                caracter = caracter.replace(letraGriega, '')
        for variable in variables:
            if variable in reemplazo:
                resultado = resultado.replace(variable, caracter)

        return resultado


    #Ejecución de hileras
    def ejecutarHilera(self, algoritmo, hilera): 

        gramatica = Gramatica.Gramatica()        
        
        gramatica.setMarcadores(self.compilamarcadores(algoritmo))

        gramatica.setComentarios(self.compilacomentarios(algoritmo))

        gramatica.setSimbolos(self.compilasimbolos(algoritmo))
        gramatica.setVariables(self.compilavariables(algoritmo))

        self.creaRgxReglas(gramatica)  

        gramatica.setReglas(self.creaListaDeElementosRegla(algoritmo, gramatica.getVariables(), gramatica.getSimbolos()))
        
        if len(gramatica.getReglas()) <= 0:
            return 'Error: There have a problem to Run, please check the algorithm'

        return self.reemplaza(hilera, gramatica)

    def ejecutarHileraDebug(self, algoritmo, hilera): 

        gramatica = Gramatica.Gramatica()        
        
        gramatica.setMarcadores(self.compilamarcadores(algoritmo))

        gramatica.setComentarios(self.compilacomentarios(algoritmo))

        gramatica.setSimbolos(self.compilasimbolos(algoritmo))

        gramatica.setVariables(self.compilavariables(algoritmo))

        self.creaRgxReglas(gramatica)
        gramatica.setReglas(self.creaListaDeElementosRegla(algoritmo, gramatica.getVariables(), gramatica.getSimbolos()))

        if len(gramatica.getReglas()) <= 0:
            return 'Error: There was a problem, please check the algorithm'

        return self.remplazaDebug(hilera, gramatica, self.compilaReglas(algoritmo))

        
        