class Regla:
    def __init__(self, etiqueta, patron, reemplazo, instruccion, terminal):
        self.__etiqueta = etiqueta.strip()
        self.__patron = patron
        self.__reemplazo = reemplazo
        self.__instruccion = instruccion.strip()
        self.__terminal = terminal

    def getEtiqueta(self):
        return self.__etiqueta

    def getPatron(self):
        return self.__patron

    def getReemplazo(self):
        return self.__reemplazo

    def getInstruccion(self):
        return self.__instruccion

    def getTerminal(self):
        return self.__terminal

    def setEtiqueta(self, valor):
            self.__etiqueta = valor.strip()

    def setPatron(self, valor):
            self.__patron = valor

    def setReemplazo(self, valor):
            self.__reemplazo = valor

    def setInstruccion(self, valor):
            self.__instruccion = valor.strip()

    def setTerminal(self, valor):
            self.__terminal= valor