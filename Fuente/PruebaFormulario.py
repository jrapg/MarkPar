from tkinter import ttk
import tkinter as tk

class Application(ttk.Frame):
    
    def __init__(self, main_window):
        super().__init__(main_window)
        main_window.title("Ventana")
        self.prueba_label=tk.Label(self, text="Ejecución de un algoritmo", font='Helvetica 18 bold')
        self.prueba_label.grid(row=0, column=0)
        self.label_hilera=tk.Label(self, text="Hilera:")
        self.label_hilera.grid(row=1, column=0)
        self.entrada_hilera=tk.Entry(self)
        self.entrada_hilera.grid(row=1, column=1)
        self.label_algoritmo=tk.Label(self,text="Algoritmo:")
        self.label_algoritmo.grid(row=2, column=0)
        self.combo = ttk.Combobox(self)
        self.combo.grid(row=2, column=1)
        self.combo["values"] = ["Invertir", "Duplicar", "Es Palíndromo", "Convertir a binario"]
        self.label_salida=tk.Label(self, text="Salida:")
        self.label_salida.grid(row=3, column=0)
        self.entrada_salida=tk.Entry(self)
        self.entrada_salida.grid(row=3, column=1)
        self.button = tk.Button(self, text="Run")
        self.button.grid(row=4, column=0)
        main_window.configure(width=600, height=300)
        self.place(width=600, height=300)
       

main_window = tk.Tk()
app = Application(main_window)

app.mainloop()